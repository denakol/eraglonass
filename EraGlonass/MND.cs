﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EraGlonass
{
    [Serializable]
    public class MND
    {
        public Int32 Id { get; private set; }
        public Int32 MessageIdentifer { get; private set; }
        public Boolean AutomaticActivation { get; private set; }
        public Boolean TestCall { get; private set; }
        public Boolean PositionCanBeTrusted { get; private set; }

        public Boolean M1 { get; private set; }
        public Boolean M2 { get; private set; }
        public Boolean M3 { get; private set; }
        public Boolean N1 { get; private set; }
        public Boolean N2 { get; private set; }
        public Boolean N3 { get; private set; }
        public Boolean L1e { get; private set; }
        public Boolean L2e { get; private set; }
        public Boolean L3e { get; private set; }
        public Boolean L4e { get; private set; }
        public Boolean L5e { get; private set; }
        public Boolean L6e { get; private set; }
        public Boolean L7e { get; private set; }

        public String VenicleIdentification { get; private set; }

        public Int32 VeniclePropulsion { get; private set; }
        public Int32 TimeStamp { get; private set; }
        public Int32 VenicleLocationLatitude { get; private set; }
        public Int32 VenicleLocationlongitude { get; private set; }
        public Int32 VenicleDirection { get; private set; }

        public Int32 RecentVenicleLocationLatitude { get; private set; }
        public Int32 RecentVenicleLocationlongitude { get; private set; }
        public Int32 RRecentVenicleLocationLatitude { get; private set; }
        public Int32 RRecentVenicleLocationlongitude { get; private set; }

        public Int32 NumberOfPassenges { get; private set; }

        public String OptionalAdditionalData { get; private set; }

        
        public MND(byte[] message)
        {
            Int32 position = 0;
            Id = BitConverter.ToInt32(message, position);
            position += 4;
            MessageIdentifer = BitConverter.ToInt32(message, position);
             position+=4;
            AutomaticActivation = BitConverter.ToBoolean(message, position);
            position++;
            TestCall = BitConverter.ToBoolean(message, position);
            position++;
            PositionCanBeTrusted = BitConverter.ToBoolean(message, position);
            position++;
            M1 = BitConverter.ToBoolean(message, position);
            position++;
            M2 = BitConverter.ToBoolean(message, position);
            position++;
            M3 = BitConverter.ToBoolean(message, position);
            position++;
            N1 = BitConverter.ToBoolean(message, position);
            position++;
            N2 = BitConverter.ToBoolean(message, position);
            position++;
            N3 = BitConverter.ToBoolean(message, position);
            position++;
            L1e = BitConverter.ToBoolean(message, position);
            position++;
            L2e = BitConverter.ToBoolean(message, position);
            position++;
            L3e = BitConverter.ToBoolean(message, position);
            position++;
            L4e = BitConverter.ToBoolean(message, position);
            position++;
            L5e = BitConverter.ToBoolean(message, position);
            position++;
            L6e = BitConverter.ToBoolean(message, position);
            position++;
            L7e = BitConverter.ToBoolean(message, position);
            position++;
            VenicleIdentification = Encoding.ASCII.GetString(message, position, 17);
            position += 17;
            VeniclePropulsion = BitConverter.ToInt32(message, position);
            position+=4;
            TimeStamp = BitConverter.ToInt32(message, position);
            position += 4;
            VenicleLocationLatitude = BitConverter.ToInt32(message, position);
            position += 4;
            VenicleLocationlongitude = BitConverter.ToInt32(message, position);
            position += 4;
            VenicleDirection = BitConverter.ToInt32(message, position);
            position += 4;
            RecentVenicleLocationLatitude = BitConverter.ToInt32(message, position);
            position += 4;
            RecentVenicleLocationlongitude = BitConverter.ToInt32(message, position);
            position += 4;
            RRecentVenicleLocationLatitude = BitConverter.ToInt32(message, position);
            position += 4;
            RRecentVenicleLocationlongitude = BitConverter.ToInt32(message, position);
            position += 4;
            NumberOfPassenges = BitConverter.ToInt32(message, position);
            position+=4;
            OptionalAdditionalData = Encoding.ASCII.GetString(message, position, 103);
            position += 103;

        }

    }
}
