﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using LibraryNTC;

namespace EraGlonass
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly Progress<String> progress = new Progress<string>(); 
        private readonly EraGlonassReciver reciver = new EraGlonassReciver();

        public MainWindow()
        {
            InitializeComponent();
            datagrid_Message.ItemsSource = reciver.Message;
            AtOnceSave.IsChecked = Properties.Settings.Default.AtOnceSave;
            progress.ProgressChanged += Log;
        }

        private void btn_Setting_Click(object sender, RoutedEventArgs e)
        {
            var window = new Setting_Window();
            window.ShowDialog();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new Microsoft.Win32.OpenFileDialog
            {
                FileName = "Document",
                DefaultExt = ".mnd",
                Filter = "Сообщения МНД (.mnd)|*.mnd"
            };
            var result = dlg.ShowDialog();
            if (result == true)
            {
                string fileName = dlg.FileName;
                using (var fileStream = File.Create(fileName))
                {
                    var serObject = new byte[fileStream.Length];
                    await fileStream.ReadAsync(serObject, 0, serObject.Length);
                    reciver.Message = SerizableByteArray<ObservableCollection<MND>>.Deserialize(serObject);
                }
            }
        }

        private async void btn_Save_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new Microsoft.Win32.SaveFileDialog
            {
                FileName = "Document",
                DefaultExt = ".mnd",
                Filter = "Сообщения МНД (.mnd)|*.mnd"
            };
            bool? result = dlg.ShowDialog();
            if (result == true)
            {
                string fileName = dlg.FileName;
                using (var fileStram = File.Create(fileName))
                {
                    var serObject = SerizableByteArray<ObservableCollection<MND>>.Serialize(reciver.Message);
                    await fileStram.WriteAsync(serObject, 0, serObject.Length);
                }
            }
        }

        private async void btn_Receive_Click(object sender, RoutedEventArgs e)
        {
            if (Properties.Settings.Default.PortName.Length != 0)
            {
                await reciver.Receive(progress);
                if (Properties.Settings.Default.AtOnceSave)
                {
                    btn_Save_Click(sender, e);
                }
            }
            else
            {
                MessageBox.Show("Задайте имя порта");
            }
        }

        private void Log(Object obj, String message)
        {
            txt_Log.Text += message + "\n";
        }
       
        private void AtOnceSave_Checked(object sender, RoutedEventArgs e)
        {
            if (AtOnceSave.IsChecked != null) Properties.Settings.Default.AtOnceSave = AtOnceSave.IsChecked.Value;
        }
    }
}
