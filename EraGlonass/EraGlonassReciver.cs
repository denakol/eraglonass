﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EraGlonass
{
    public  class EraGlonassReciver
    {
        public ObservableCollection<MND> Message = new ObservableCollection<MND>();

        public async Task Receive(IProgress<String> progress)
        {
                var port = new SerialPort
                {
                    BaudRate = Properties.Settings.Default.BaudRate,
                    Parity = (Parity)Enum.Parse(typeof(Parity), Properties.Settings.Default.Parity),
                    StopBits = (StopBits)Enum.Parse(typeof(StopBits), Properties.Settings.Default.StopBits),
                    PortName = Properties.Settings.Default.PortName
                };
                progress.Report("Открытие порта");
                try
                {
                    port.Open();
                }
                catch (System.UnauthorizedAccessException ex)
                {
                    progress.Report("Отказ в доступе к порту или для текущего процесса или другого процессав системе уже открыт заданный порт COM.");
                    return;
                }
                catch (System.IO.IOException ex)
                {
                    progress.Report(String.Format("Порт находится в недействительном состоянии. {0}", ex.Message));
                    return;
                }

                progress.Report("Порт открыт");
                progress.Report("Чтение сообщений");
                try
                {
                    using (port)
                    {
                        await ReadMessage(port,progress);
                    }
                }
                catch (System.TimeoutException ex)
                {
                    progress.Report("Отсутствуют байты, доступные для чтения.");
                     return;
                }
                progress.Report("Завершено");
            
        }

        private async Task ReadMessage(SerialPort port, IProgress<String> progress )
        {

            Int16 endMessage = Properties.Settings.Default.StopReceive;
            var buf = new byte[170];
            var count = 1;
            var task = port.BaseStream.WriteAsync(BitConverter.GetBytes(Properties.Settings.Default.Invite), 0, 2);
            if (task != await Task.WhenAny(task, Task.Delay(5000)))
            {
                throw new System.TimeoutException();
            }
            while (true)
            {
                progress.Report(String.Format("Чтение {0} сообщения", count));
                task = port.BaseStream.ReadAsync(buf, 0, 2);
                if (task != await Task.WhenAny(task, Task.Delay(5000)))
                {
                    throw new System.TimeoutException();
                }
                Int16 header = BitConverter.ToInt16(buf, 0);
                if (header != endMessage)
                {
                    progress.Report(String.Format("Прочитано сообщение о конце передачи"));
                    break;
                }
                task = port.BaseStream.ReadAsync(buf, 2, 168);
                if (task != await Task.WhenAny(task, Task.Delay(5000)))
                {
                    throw new System.TimeoutException();
                }
                //if (readLength != 144)
                //{
                //    while (readLength != 144)
                //    {
                //        readLength += port.Read(buf, readLength, 154 - readLength);
                //    }
                //}
                Message.Add(new MND(buf));
                progress.Report(String.Format("Сообщение {0} прочитано", count++));

            }
        }
    }
}
