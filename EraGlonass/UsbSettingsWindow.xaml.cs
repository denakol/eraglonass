﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.IO.Ports;

namespace gnss
{
    /// <summary>
    /// Логика взаимодействия для UsbSettings.xaml
    /// </summary>
    public partial class UsbSettingsWindow : Window
    {
        string[] baudeRate = {  "2400",
                                "4800",
                                "9600",
                                "19200",
                                "38400",
                                "57600",
                                "115200"};
        public UsbSettingsWindow()
        {
            InitializeComponent();

            cbBaudeRate.ItemsSource = baudeRate;
            cbBaudeRate.SelectedItem = EraGlonass.Properties.Settings.Default.BaudRate.ToString();

            cbParity.ItemsSource = Enum.GetValues(typeof(Parity));
            cbParity.Text = Properties.Settings.Default.Parity;

            cbStopBits.ItemsSource = Enum.GetValues(typeof(StopBits));
            cbStopBits.Text = Properties.Settings.Default.StopBits;

            cbPortName.ItemsSource = SerialPort.GetPortNames();
            cbPortName.Text = Properties.Settings.Default.PortName;

            tbMessages.Text = Properties.Settings.Default.PortMessages;
        }

        private void bSave_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.BaudRate = Convert.ToInt32(cbBaudeRate.SelectedItem);
            Properties.Settings.Default.Parity = cbParity.SelectedItem.ToString();
            Properties.Settings.Default.StopBits = cbStopBits.SelectedItem.ToString();
            Properties.Settings.Default.PortName = cbPortName.Text;

            Properties.Settings.Default.PortMessages = tbMessages.Text;

            //Properties.Settings.Default.Upgrade();
            Properties.Settings.Default.Save();

            this.Close();
        }

        private void bCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
}
